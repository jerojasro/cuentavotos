from os.path import splitext
from os.path import basename
import sys
from collections import namedtuple

from numpy import array
from scipy.spatial.distance import cityblock
from skimage.io import imread
from skimage.io import imsave

Point = namedtuple('Point', ['row', 'col'])

SEAT_COORDS = [
    # three top seats
    Point(24, 488),
    Point(27, 554),
    Point(23, 623),

    # first semicircle
    Point(46, 224),
    Point(87, 250),
    Point(124, 279),
    Point(161, 316),

    Point(218, 416),
    Point(235, 460),
    Point(242, 506),

    Point(239, 612),
    Point(231, 656),
    Point(214, 692),

    Point(150, 796),
    Point(110, 835),
    Point(68, 866),

    # second semicircle
    Point(125, 150),
    Point(169, 183),
    Point(208, 216),
    Point(242, 257),

    Point(303, 362),
    Point(319, 403),
    Point(333, 455),
    Point(334, 502),

    Point(331, 610),
    Point(327, 657),
    Point(314, 706),
    Point(295, 749),

    Point(231, 857),
    Point(195, 902),
    Point(154, 937),

    # third semicircle
    Point(214, 89),
    Point(248, 122),
    Point(285, 159),
    Point(322, 203),

    Point(388, 317),
    Point(408, 360),
    Point(424, 403),
    Point(434, 450),
    Point(439, 502),

    Point(437, 624),
    Point(431, 672),
    Point(420, 719),
    Point(405, 762),
    Point(384, 805),

    Point(310, 917),
    Point(273, 955),
    Point(235, 992),
    Point(195, 1026),

    # fourth semicircle
    Point(336, 80),
    Point(375, 118),
    Point(399, 159),

    Point(471, 272),
    Point(490, 318),
    Point(507, 367),
    Point(521, 407),
    Point(525, 452),
    Point(528, 499),

    Point(527, 620),
    Point(523, 676),
    Point(513, 722),
    Point(498, 768),
    Point(480, 811),
    Point(462, 849),

    Point(393, 967),
    Point(362, 1005),
    Point(325, 1042),

    # fifth semicircle
    Point(419, 44),
    Point(454, 85),
    Point(481, 123),

    Point(545, 229),
    Point(568, 272),
    Point(584, 317),
    Point(598, 358),
    Point(610, 403),
    Point(615, 455),
    Point(620, 502),

    Point(618, 631),
    Point(610, 683),
    Point(603, 721),
    Point(592, 770),
    Point(576, 815),
    Point(555, 858),
    Point(537, 902),

    Point(471, 1010),
    Point(440, 1051),
    Point(404, 1084),

    # sixth semicircle
    Point(617, 192),
    Point(638, 236),
    Point(658, 276),
    Point(672, 318),
    Point(682, 365),
    Point(690, 409),
    Point(701, 452),
    Point(704, 503),

    Point(704, 638),
    Point(693, 682),
    Point(686, 724),
    Point(679, 774),
    Point(664, 815),
    Point(648, 861),
    Point(630, 903),
    Point(608, 943),

    # seventh semicircle
    Point(770, 410),
    Point(778, 454),
    Point(780, 502),

    Point(779, 634),
    Point(775, 679),
    Point(767, 724),

]

YES = 'YES'
NO = 'NO'
BG = 'BG'
MISSING = 'MISSING'
PENDING = 'PENDING'
POSSIBLE_VOTE_COLORS = {
    BG: array([0x00, 0x00, 0x3c]),
    YES: array([0x09, 0x93, 0x00]),
    NO: array([0xe4, 0x00, 0x00]),
    MISSING: array([0x7c, 0x7e, 0x7e]),
    PENDING: array([0xf4, 0xfe, 0x02]),
}

def cropped_img(full_img):
    return full_img[150:947, 716:1832]


def classify_vote(img, seat_coords, sq_px_half_len=24):
    o_r = seat_coords.row
    o_c = seat_coords.col

    pixel_color_count = {BG: 0, YES: 0, NO: 0, MISSING: 0, PENDING: 0}

    for r in range(max(o_r - sq_px_half_len, 0), min(o_r + sq_px_half_len, img.shape[0] - 1) + 1):
        for c in range(max(o_c - sq_px_half_len, 0), min(o_c + sq_px_half_len, img.shape[1] - 1) + 1):
            _, PIXEL_VOTE_TAG = min(
                (cityblock(img[r, c], POSSIBLE_VOTE_COLORS[k]), k)
                for k in POSSIBLE_VOTE_COLORS
            )
            pixel_color_count[PIXEL_VOTE_TAG] += 1

    del pixel_color_count[BG]
    pixel_cnt, tag = max((v, k) for k, v in pixel_color_count.items())
    return tag


def draw_yes(img, seat_coords):
    o_r, o_c = seat_coords.row, seat_coords.col
    for c in range(o_c - 7, o_c + 7 + 1):
        img[o_r - 1, c] = [0, 0, 0]
        img[o_r, c] = [0, 0, 0]
        img[o_r + 1, c] = [0, 0, 0]

    for r in range(o_r - 7, o_r + 7 + 1):
        img[r, o_c - 1] = [0, 0, 0]
        img[r, o_c] = [0, 0, 0]
        img[r, o_c + 1] = [0, 0, 0]


def draw_no(img, seat_coords):
    o_r, o_c = seat_coords.row, seat_coords.col
    for c in range(o_c - 7, o_c + 7 + 1):
        img[o_r - 1, c] = [0, 0, 0]
        img[o_r, c] = [0, 0, 0]
        img[o_r + 1, c] = [0, 0, 0]

def draw_pending(img, seat_coords):
    o_r, o_c = seat_coords.row, seat_coords.col
    for d in range(-7, 7 + 1):
        img[o_r+d-1, o_c+d] = [0, 0, 0]
        img[o_r+d, o_c+d] = [0, 0, 0]
        img[o_r+d + 1, o_c+d] = [0, 0, 0]


def draw_square(img, sq_center_coords, sq_px_half_len=24):
    o_r = sq_center_coords.row
    o_c = sq_center_coords.col
    for c in range(max(o_c - sq_px_half_len, 0), min(o_c + sq_px_half_len, img.shape[1] - 1) + 1):
        img[max(o_r - sq_px_half_len, 0), c] = [0, 0, 0]
        img[min(o_r + sq_px_half_len, img.shape[0] - 1), c] = [0, 0, 0]

    for r in range(max(o_r - sq_px_half_len, 0), min(o_r + sq_px_half_len, img.shape[0] - 1) + 1):
        img[r, max(o_c - sq_px_half_len, 0)] = [0, 0, 0]
        img[r, min(o_c + sq_px_half_len, img.shape[1] - 1)] = [0, 0, 0]

def main():
    img = cropped_img(imread(sys.argv[1]))
    for i, point in enumerate(SEAT_COORDS):
        tag = classify_vote(img, point)
        if tag == YES:
            draw_yes(img, point)
        if tag == NO:
            draw_no(img, point)
        if tag == PENDING:
            draw_pending(img, point)
        print('{}: {}'.format(i, tag))

    fn_prefix, _ = splitext(sys.argv[1])
    imsave("{}-proc.png".format(fn_prefix), img)

if __name__ == '__main__':
    main()
