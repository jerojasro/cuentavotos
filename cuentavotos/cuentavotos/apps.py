from django.apps import AppConfig


class CuentavotosConfig(AppConfig):
    name = 'cuentavotos'
