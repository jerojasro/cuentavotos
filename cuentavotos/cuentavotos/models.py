import re

from django.db import models

from unidecode import unidecode

class Partido(models.Model):
    nombre = models.CharField(max_length=200)
    slug = models.SlugField()
    logo = models.URLField(max_length=1000)


class Congresista(models.Model):
    nombre_completo = models.CharField(max_length=100)


class CongresistaEnPeriodo(models.Model):
    congresista = models.ForeignKey('cuentavotos.Congresista', models.PROTECT)
    cuerpo_legislativo = models.CharField(
        max_length=1,
        choices=[('S', 'Senado'), ('C', 'Cámara')],
    )
    partido = models.ForeignKey('cuentavotos.Partido', models.PROTECT)
    fecha_inicial = models.DateField()
    fecha_final = models.DateField()


class DisposicionRecinto(models.Model):
    cuerpo_legislativo = models.CharField(
        max_length=1,
        choices=[('S', 'Senado'), ('C', 'Cámara')],
    )
    fecha_inicial = models.DateField()
    fecha_final = models.DateField()


class AsientoCongresista(models.Model):
    posicion = models.IntegerField(help_text=(
        'Número de secuencia para darle un nombre'
        ' al sitio de cada congresista'
    ))
    disposicion_recinto = models.ForeignKey('cuentavotos.DisposicionRecinto', models.PROTECT)
    periodo_congresista = models.ForeignKey('cuentavotos.CongresistaEnPeriodo', models.PROTECT)
    coord_x = models.IntegerField()
    coord_y = models.IntegerField()


def name_for_frame(votacion, fn):
    not_alnum_re = re.compile(r'[^a-zA-Z0-9]')
    safe_topic = unidecode(votacion.tema)
    safe_topic, _ = not_alnum_re.subn('', safe_topic)
    return 'votaciones/{}/{}-{}.png'.format(
        votacion.fecha_voto.isoformat()[:7],  # yyyy-mm
        votacion.fecha_voto.isoformat(),
        safe_topic[:20],
    )

class Votacion(models.Model):
    tema = models.CharField(max_length=5000)
    fecha_voto = models.DateField(help_text='fecha de la votación en el congreso')
    video_url = models.URLField(max_length=5000)
    pantallazo = models.FileField(upload_to=name_for_frame)
    fecha_registro = models.DateTimeField(
        help_text='fecha en que la votación fue registrada en este sistema',
        auto_now_add=True,
    )


class Voto(models.Model):
    votacion = models.ForeignKey('cuentavotos.Votacion', models.PROTECT)
    congresista_en_periodo = models.ForeignKey('cuentavotos.CongresistaEnPeriodo', models.PROTECT)
    voto = models.CharField(
        max_length=1,
        choices=[
            ('S', 'Sí'),
            ('N', 'No'),
            ('A', 'Ausente'),
            ('P', 'Pendiente'),
        ],
    )


