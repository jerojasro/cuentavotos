Extraer un pantallazo a partir del video:

  1. obtener URL del flujo de video
  1. descargar 1 segundo del flujo de video
  1. obtener una imagen a partir del video descargado

Detalles:

    $ youtube-dl --get-url 'https://www.youtube.com/watch?v=oMb69wrSQGc' | head -1
    https://r3---sn-xuc-cvbd.googlevideo.com/videoplayback?ms=au%2Crdu&id=o-ACzojnhF_Kc6E70qnXn07fUAps33BL1uk5p_mTygU5Oc&pl=22&mv=m&mt=1530065956&signature=B8BDD5CCD4DAF0A8AB1595FC54FF50CBEE09BBAF.2CD7500CDED2E2D6BDD6A1A1F7C0E04838628FDC&ipbits=0&key=yt6&ip=190.27.91.82&initcwndbps=316250&mn=sn-xuc-cvbd%2Csn-cvb7ln7z&mm=31%2C29&ratebypass=yes&c=WEB&source=youtube&fvip=3&lmt=1529745168031802&expire=1530087663&pcm2cms=yes&mime=video%2Fmp4&itag=22&ei=jvQyW9nWNtCukAS5ooWgBQ&fexp=23709359&dur=15321.594&sparams=dur%2Cei%2Cgcr%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpcm2cms%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cexpire&gcr=co&requiressl=yes

Ese comando entrega dos URLs, sólo nos interesa la primera, que tiene el flujo
de video.

Descargue un segundo (-t 1) del flujo de video, a partir de 2h17m14s:

    ffmpeg -ss 2:17:14 -t 1 -i 'https://r3---sn-xuc-cvbd.googlevideo.com/videoplayback?expire=1530088032&lmt=1529741830507115&c=WEB&ei=APYyW5uJHo72qQWNspDQDg&aitags=133%2C134%2C135%2C136%2C137%2C160%2C242%2C243%2C244%2C247%2C248%2C278&ipbits=0&clen=1915070353&key=yt6&requiressl=yes&sparams=aitags%2Cclen%2Cdur%2Cei%2Cgcr%2Cgir%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Ckeepalive%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Crequiressl%2Csource%2Cexpire&gir=yes&keepalive=yes&gcr=co&fexp=23709359&dur=15321.537&fvip=3&initcwndbps=316250&id=o-ACd6wbvDUcUOjIyrZhkunwPFj_Kdev-A9SBiikcXXdiN&pl=22&source=youtube&mv=m&mime=video%2Fmp4&ms=au%2Crdu&signature=26BC45B1E77D75B1CAE2317AC7410EB867954703.188BBC7F9140DB003828EF8A4699D7873B7E702F&mm=31%2C29&ip=190.27.91.82&mn=sn-xuc-cvbd%2Csn-cvb7ln7z&mt=1530066351&itag=137&ratebypass=yes' -map 0:v  output.mkv

Genere un archivo PNG con la primera trama del flujo:

    ffmpeg -i output.mkv -vframes 1 out.png
